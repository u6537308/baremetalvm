use crate::{BaremetalVM, OBJ_SIZES};
use mmtk::util::OpaquePointer;
use mmtk::util::{Address, ObjectReference};
use mmtk::vm::{ObjectModel, VMBinding, ActivePlan};
use mmtk::Allocator;
use std::sync::atomic::{AtomicU8, Ordering, AtomicUsize};
use mmtk::CollectorContext;
use std::sync::RwLockWriteGuard;
use std::collections::HashMap;

pub struct VMObjectModel {}

impl ObjectModel<BaremetalVM> for VMObjectModel {
    const GC_BYTE_OFFSET: usize = 0;

    fn get_gc_byte(object: ObjectReference) -> &'static AtomicU8 {
        unsafe {
            let gc_byte = &*(object.to_address() + Self::GC_BYTE_OFFSET / 8).to_ptr::<AtomicU8>();
            gc_byte
        }
    }

    fn copy(from: ObjectReference, allocator: Allocator, tls: OpaquePointer) -> ObjectReference {
        let obj_sizes: RwLockWriteGuard<HashMap<usize, usize>> =
            OBJ_SIZES.write().unwrap();

        let bytes = obj_sizes.get(&from.to_address().as_usize()).unwrap();

        let context  = unsafe { <BaremetalVM as VMBinding>::VMActivePlan::collector(tls) };
        let dst = context.alloc_copy(from, *bytes, ::std::mem::size_of::<usize>(), 0, allocator);
        let src = from.to_address();
        for i in 0..*bytes {
            unsafe { (dst + i).store((src + i).load::<u8>()) };
        }
        let to_obj = unsafe { dst.to_object_reference() };
        context.post_copy(to_obj, unsafe { Address::zero() }, *bytes, allocator);
        to_obj

    }

    fn copy_to(_from: ObjectReference, _to: ObjectReference, _region: Address) -> Address {
        unimplemented!()
    }

    fn get_reference_when_copied_to(_from: ObjectReference, _to: Address) -> ObjectReference {
        unimplemented!()
    }

    fn get_size_when_copied(_object: ObjectReference) -> usize {
        unimplemented!()
    }

    fn get_align_when_copied(_object: ObjectReference) -> usize {
        unimplemented!()
    }

    fn get_align_offset_when_copied(_object: ObjectReference) -> isize {
        unimplemented!()
    }

    fn get_current_size(_object: ObjectReference) -> usize {
        unimplemented!()
    }

    fn get_next_object(_object: ObjectReference) -> ObjectReference {
        unimplemented!()
    }

    unsafe fn get_object_from_start_address(_start: Address) -> ObjectReference {
        unimplemented!()
    }

    fn get_object_end_address(_object: ObjectReference) -> Address {
        unimplemented!()
    }

    fn get_type_descriptor(_reference: ObjectReference) -> &'static [i8] {
        unimplemented!()
    }

    fn is_array(_object: ObjectReference) -> bool {
        unimplemented!()
    }

    fn is_primitive_array(_object: ObjectReference) -> bool {
        unimplemented!()
    }

    fn get_array_length(_object: ObjectReference) -> usize {
        unimplemented!()
    }

    fn attempt_available_bits(_object: ObjectReference, _old: usize, _new: usize) -> bool {
        unimplemented!()
    }

    fn prepare_available_bits(object: ObjectReference) -> usize {
        unsafe { object.to_address().load() }
    }

    fn write_available_byte(_object: ObjectReference, _val: u8) {
        unimplemented!()
    }

    fn read_available_byte(_object: ObjectReference) -> u8 {
        unimplemented!()
    }

    fn write_available_bits_word(object: ObjectReference, val: usize) {
        unsafe { object.to_address().atomic_store::<AtomicUsize>(val, Ordering::SeqCst) }
    }

    fn read_available_bits_word(object: ObjectReference) -> usize {
        unsafe { object.to_address().atomic_load::<AtomicUsize>(Ordering::SeqCst) }
    }

    fn gc_header_offset() -> isize {
        unimplemented!()
    }

    fn object_start_ref(_object: ObjectReference) -> Address {
        unimplemented!()
    }

    fn ref_to_address(object: ObjectReference) -> Address {
        object.to_address()
    }

    fn is_acyclic(_typeref: ObjectReference) -> bool {
        unimplemented!()
    }

    fn dump_object(_object: ObjectReference) {
        unimplemented!()
    }

    fn get_array_base_offset() -> isize {
        unimplemented!()
    }

    fn array_base_offset_trapdoor<T>(_object: T) -> isize {
        unimplemented!()
    }

    fn get_array_length_offset() -> isize {
        unimplemented!()
    }
}
