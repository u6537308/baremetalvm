use crate::{BaremetalVM,UPCALLS};
use mmtk::util::OpaquePointer;
use mmtk::vm::Collection;
use mmtk::{MutatorContext, ParallelCollector};
use crate::{BLOCK_FOR_GC, STW_COND};
use std::sync::atomic::{AtomicBool, Ordering};

pub struct VMCollection {}

impl Collection<BaremetalVM> for VMCollection {
    fn stop_all_mutators(_tls: OpaquePointer) {
    }

    fn resume_mutators(_tls: OpaquePointer) {
	AtomicBool::store(&BLOCK_FOR_GC, false, Ordering::SeqCst);
        let &(_, ref cvar ) = &*STW_COND.clone();
        cvar.notify_all();
    }

    fn block_for_gc(_tls: OpaquePointer) {
	AtomicBool::store(&BLOCK_FOR_GC, true, Ordering::SeqCst);
    }

    fn spawn_worker_thread<T: ParallelCollector<BaremetalVM>>(tls: OpaquePointer, ctx: Option<&mut T>) {
	let ctx_ptr = if let Some(r) = ctx {
            r as *mut T
        } else {
            std::ptr::null_mut()
        };
        unsafe {
            ((*UPCALLS).spawn_collector_thread)(tls, ctx_ptr as usize as _);
        }
    }

    fn prepare_mutator<T: MutatorContext<BaremetalVM>>(_tls: OpaquePointer, _mutator: &T) {
	
    }
}
