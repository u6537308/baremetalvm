use mmtk::vm::Scanning;
use mmtk::{TransitiveClosure, TraceLocal};
use mmtk::util::{ObjectReference, SynchronizedCounter};
use mmtk::util::{OpaquePointer, Address};
use crate::ROOTS;
use std::sync::MutexGuard;
use crate::BaremetalVM;

static COUNTER: SynchronizedCounter = SynchronizedCounter::new(0);

pub struct VMScanning {}

impl Scanning<BaremetalVM> for VMScanning {
    fn scan_object<T: TransitiveClosure>(_trace: &mut T, _object: ObjectReference, _tls: OpaquePointer) {
        
    }

    fn reset_thread_counter() {
        COUNTER.reset();
    }

    fn notify_initial_thread_scan_complete(_partial_scan: bool, _tls: OpaquePointer) {
        
    }

    fn compute_static_roots<T: TraceLocal>(_trace: &mut T, _tls: OpaquePointer) {
        
    }

    fn compute_global_roots<T: TraceLocal>(_trace: &mut T, _tls: OpaquePointer) {
        
    }

    fn compute_thread_roots<T: TraceLocal>(trace: &mut T, _tls: OpaquePointer) {
        let mut roots : MutexGuard<Vec<Address>> = ROOTS.lock().unwrap();
        for addr in roots.drain(..) {
            println!("Processing root {:?}", addr);
            trace.process_root_edge(addr, true);
        }

    }

    fn compute_new_thread_roots<T: TraceLocal>(_trace: &mut T, _tls: OpaquePointer) {
        unimplemented!()
    }

    fn compute_bootimage_roots<T: TraceLocal>(_trace: &mut T, _tls: OpaquePointer) {
    
    }

    fn supports_return_barrier() -> bool {
        unimplemented!()
    }
}
