extern crate mmtk;
extern crate libc;
#[macro_use]
extern crate lazy_static;

use mmtk::util::{Address, OpaquePointer};
use mmtk::vm::VMBinding;
use mmtk::{Plan, MMTK};
use std::ptr::null_mut;
use std::sync::atomic::AtomicBool;
use mmtk::SelectedPlan;
use std::collections::HashMap;
use std::sync::{Arc, Condvar, Mutex, RwLock};



pub mod scanning;
pub mod collection;
pub mod object_model;
pub mod active_plan;
pub mod reference_glue;
pub mod api;

pub struct BaremetalVM;

impl VMBinding for BaremetalVM {
    type VMObjectModel = object_model::VMObjectModel;
    type VMScanning = scanning::VMScanning;
    type VMCollection = collection::VMCollection;
    type VMActivePlan = active_plan::VMActivePlan;
    type VMReferenceGlue = reference_glue::VMReferenceGlue;
}

//#[cfg(feature = "baremetalvm")]
lazy_static! {
    pub static ref SINGLETON: MMTK<BaremetalVM> = MMTK::new();
    pub static ref BLOCK_FOR_GC: AtomicBool = AtomicBool::new(false);
    pub static ref STW_COND: Arc<(Mutex<usize>, Condvar)> = { Arc::new((Mutex::new(0), Condvar::new())) };
    pub static ref ROOTS: Mutex<Vec<Address>> = { Mutex::new(vec![]) };
    pub static ref OBJ_SIZES: RwLock<HashMap<usize, usize>> = RwLock::new(HashMap::new());

}

#[repr(C)]
pub struct Baremetal_Upcalls {
    pub spawn_collector_thread: extern "C" fn(tls: OpaquePointer, ctx: *mut <SelectedPlan<BaremetalVM> as Plan<BaremetalVM>>::CollectorT),
    pub get_collector_from_tls: extern "C" fn(tls: OpaquePointer) -> *mut <SelectedPlan<BaremetalVM> as Plan<BaremetalVM>>::CollectorT,
    pub get_mutator: extern "C" fn() -> *mut <SelectedPlan<BaremetalVM> as Plan<BaremetalVM>>::MutatorT,
    pub reset_get_mutator: extern "C" fn(),
    pub get_thread_frame_pointer: extern "C" fn() -> Address,
}

pub static mut UPCALLS: *const Baremetal_Upcalls = null_mut();

