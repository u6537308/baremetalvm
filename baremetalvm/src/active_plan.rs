use SINGLETON;
use crate::{BaremetalVM,UPCALLS};
use mmtk::{Plan, SelectedPlan};
use mmtk::vm::ActivePlan;
use mmtk::util::OpaquePointer;

pub struct VMActivePlan<> {}

impl ActivePlan<BaremetalVM> for VMActivePlan {
    fn global() -> &'static SelectedPlan<BaremetalVM> {
        &SINGLETON.plan
    }

    unsafe fn collector(tls: OpaquePointer) -> &'static mut <SelectedPlan<BaremetalVM> as Plan<BaremetalVM>>::CollectorT {
        let c = ((*UPCALLS).get_collector_from_tls)(tls);
        &mut *c
    }

    unsafe fn is_mutator(_tls: OpaquePointer) -> bool {
        true
    }

    unsafe fn mutator(_tls: OpaquePointer) -> &'static mut <SelectedPlan<BaremetalVM> as Plan<BaremetalVM>>::MutatorT {
        let c = ((*UPCALLS).get_mutator)();
        ((*UPCALLS).reset_get_mutator)();
        &mut *c
    }

    fn collector_count() -> usize {
        unimplemented!()
    }

    fn reset_mutator_iterator() {
        unsafe {((*UPCALLS).reset_get_mutator)()}
    }

    fn get_next_mutator() -> Option<&'static mut <SelectedPlan<BaremetalVM> as Plan<BaremetalVM>>::MutatorT> {
        unsafe {
            let c = ((*UPCALLS).get_mutator)();
            if c.is_null() {
                None
            } else {
                Some(&mut *c)
            }
        }

    }
}
